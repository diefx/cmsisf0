/**------------------------------------------------------------------------------------------------
* @author  Hotboards Team
* @version v2.0.0
* @date    26-November-2017
* @brief   Just a simple wrapper to call the cmsis layer from a more suitable name.
-----------------------------------------------------------------------------------------------*/
#ifndef __CMSIS_H
#define __CMSIS_H

#ifdef __cplusplus
    extern "C" {
#endif

/*----include stm32f0 layer*/
#include "stm32f0xx.h"

/*----Interrupts vector number assigned, read the micro header family*/
#define _hal_wwdg_irq                      WWDG_IRQn


/*----rutine for interrupt request redefinitions startup_stm32f0xxxx.h*/
#define hal_irq_nmi                         NMI_Handler
#define hal_irq_hardFault                   HardFault_Handler
#define hal_irq_sysTick                     SysTick_Handler
#define hal_irq_wwdg                        WWDG_IRQHandler


#ifdef __cplusplus
    }
#endif

#endif

