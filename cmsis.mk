# Compile startup files and also include register definitions
# CMSISF0 should be defined on global makefile 

STM = F0

VPATH += $(CMSIS)/startups

INCLUDES += -I $(CMSIS) 
INCLUDES += -I $(CMSIS)/core 
INCLUDES += -I $(CMSIS)/registers

OBJ += system_stm32f0xx.o

#----Select the right startup file according to microcontroller selected---------

#define STM32F030x6 for STM32F030x4, STM32F030x6 Devices (STM32F030xx microcontrollers where the Flash memory ranges between 16 and 32 Kbytes)
ifneq (,$(filter $(MCU),stm32f030f4 stm32f030c6 stm32f030k6))
	OBJ   += startup_stm32f030x6.o
	SYMBOLS += -DSTM32F030x6
#define STM32F030x8 for STM32F030x8 Devices (STM32F030xx microcontrollers where the Flash memory is 64 Kbytes)
else ifneq (,$(filter $(MCU),stm32f030c8 stm32f030r8))
	OBJ   += startup_stm32f030x8.o
	SYMBOLS += -DSTM32F030x8
#define STM32F030xC for STM32F030xC Devices (STM32F030xC microcontrollers where the Flash memory is 256 Kbytes)
else ifneq (,$(filter $(MCU),stm32f030cc stm32f030rc))
	OBJ   += startup_stm32f030xc.o
	SYMBOLS += -DSTM32F030xC
#define STM32F031x6 for STM32F031x4, STM32F031x6 Devices (STM32F031xx microcontrollers where the Flash memory ranges between 16 and 32 Kbytes)
else ifneq (,$(filter $(MCU),stm32f031c4 stm32f031c6 stm32f031f4 stm32f031f6 stm32f031g4 stm32f031g6 stm32f031k4 stm32f031k6))
	OBJ   += startup_stm32f031x6.o
	SYMBOLS += -DSTM32F031x6
#define STM32F038xx for STM32F038xx Devices (STM32F038xx microcontrollers where the Flash memory is 32 Kbytes)
else ifneq (,$(filter $(MCU),stm32f038c6 stm32f038f6 stm32f038g6 stm32f038k6))
	OBJ   += startup_stm32f038xx.o
	SYMBOLS += -DSTM32F038xx
#define STM32F042x6 for STM32F042x4, STM32F042x6 Devices (STM32F042xx microcontrollers where the Flash memory ranges between 16 and 32 Kbytes)
else ifneq (,$(filter $(MCU),stm32f042c4 stm32f042c6 stm32f042f4 stm32f042f6 stm32f042g4 stm32f042g6 stm32f042k4 stm32f042k6 stm32f042t6))
	OBJ   += startup_stm32f046x6.o
	SYMBOLS += -DSTM32F042x6
#define STM32F048x6 for STM32F048xx Devices (STM32F042xx microcontrollers where the Flash memory is 32 Kbytes)
else ifneq (,$(filter $(MCU),stm32f048c6 stm32f048g6 stm32f048t6))
	OBJ   += startup_stm32f048xx.o
	SYMBOLS += -DSTM32F048x6
#define STM32F051x8 for STM32F051x4, STM32F051x6, STM32F051x8 Devices (STM32F051xx microcontrollers where the Flash memory ranges between 16 and 64 Kbytes)
else ifneq (,$(filter $(MCU),stm32f051c4 stm32f051c6 stm32f051c8 stm32f051k4 stm32f051k6 stm32f051k8 stm32f051r4 stm32f051r6 stm32f051r8))
	OBJ   += startup_stm32f051x8.o
	SYMBOLS += -DSTM32F051x8
#define STM32F058xx for STM32F058xx Devices (STM32F058xx microcontrollers where the Flash memory is 64 Kbytes)
else ifneq (,$(filter $(MCU),stm32f058c8 stm32f058r8))
	OBJ   += startup_stm32f058xx.o
	SYMBOLS += -DSTM32F058xx
#define STM32F070x6 for STM32F070x6 Devices (STM32F070x6 microcontrollers where the Flash memory ranges between 16 and 32 Kbytes)
else ifneq (,$(filter $(MCU),stm32f070c6 stm32f070f6))
	OBJ   += startup_stm32f070x6.o
	SYMBOLS += -DSTM32F070x6
#define STM32F070xB for STM32F070xB Devices (STM32F070xB microcontrollers where the Flash memory ranges between 64 and 128 Kbytes)
else ifneq (,$(filter $(MCU),stm32f070cb stm32f070rb))
	OBJ   += startup_stm32f070xb.o
	SYMBOLS += -DSTM32F070xB
#define STM32F071xB for STM32F071x8, STM32F071xB Devices (STM32F071xx microcontrollers where the Flash memory ranges between 64 and 128 Kbytes)
else ifneq (,$(filter $(MCU),stm32f071cb stm32f071rb stm32f071vb stm32f071v8))
	OBJ   += startup_stm32f071xb.o
	SYMBOLS += -DSTM32F071xB
#define STM32F072xB for STM32F072x8, STM32F072xB Devices (STM32F072xx microcontrollers where the Flash memory ranges between 64 and 128 Kbytes)
else ifneq (,$(filter $(MCU),stm32f072cb stm32f072rb stm32f072vb))
	OBJ   += startup_stm32f072xb.o
	SYMBOLS += -DSTM32F072xB
#define STM32F078xx for STM32F078xx Devices (STM32F078xx microcontrollers where the Flash memory is 128 Kbytes)
else ifneq (,$(filter $(MCU),stm32f078cb stm32f078rb stm32f078vb))
	OBJ   += startup_stm32f078xx.o
	SYMBOLS += -DSTM32F078xx
#define STM32F091xC for STM32F091xC Devices (STM32F091xx microcontrollers where the Flash memory is 256 Kbytes)
else ifneq (,$(filter $(MCU),stm32f091cb stm32f091cc stm32f091rb stm32f091rc stm32f091vb stm32f091vc))
	OBJ   += startup_stm32f091xc.o
	SYMBOLS += -DSTM32F091xx
#define STM32F098xx for STM32F098xx Devices (STM32F098xx microcontrollers where the Flash memory is 256 Kbytes)
else ifneq (,$(filter $(MCU),stm32f098cb stm32f098rb stm32f098vb))
	OBJ += startup_stm32f098xx.o
	SYMBOLS += -DSTM32F098xx
endif

#----select the right linkerfile according to MCU variable----------------------
ifndef $(LINKERFILE)
	LINKERFILE = $(CMSIS)/linkers/$(MCU).ld
endif

